<?php

namespace App\Jobs;

use Exception;
use App\Model\Video;
use App\VideoProcessador;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DownloadVideo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $video;

    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(VideoProcessador $processor)
    {   
        // $localFile = storage_path().'movies';
        // $image = file_get_contents($this->video->url_to_download);
        // $caminho = public_path('avatars').'/video.mp4';
        // file_put_contents($caminho, $image);

        // if($finaliza){
            $processor->download($this->video->id);
            // $video->url_video_serve = $caminho;
            // $video->status_id = 1;
            // $video->save();
        // }
    }

    public function failed(Exception $exception)
    {
        // Send user notification of failure, etc...
    }

}
