<?php

namespace App;

use App\Model\Video;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;

class VideoProcessador
{
    use Queueable, SerializesModels;

    private $video;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function download1($file_source, $file_target) {
        $rh = fopen($file_source, 'rb');
        $wh = fopen($file_target, 'w+b');
        if (!$rh || !$wh) {
            return false;
        }
    
        while (!feof($rh)) {
            if (fwrite($wh, fread($rh, 4096)) === FALSE) {
                return false;
            }
            echo ' ';
            flush();
        }
    
        fclose($rh);
        fclose($wh);
    
        return true;
    }
    public function download($id = null)
    {   
        $video = Video::find($id);
        $nameFile =  Crypt::encryptString($video->titulo);
        $localFile = public_path('movies').'/'.$nameFile.'.mp4';

        $result = $this->download1($video->url_to_download,$localFile);

        // $image = file_get_contents($video->url_to_download);
        // $isDownload = file_put_contents($localFile, $image);
        if($result){

            $video->status_id = 1;
            $video->url_video_serve = 'movies/'.$nameFile.'.mp4';
            return $video->save();
        }
    }
}
