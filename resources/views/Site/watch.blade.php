<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title> {{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        
        <!-- Styles -->
        <link href="{{ asset('css/style.scss') }}" rel="stylesheet" >     
        <link href="{{ asset('css/player.css') }}" rel="stylesheet" >

    </head>

    <style>
            #instructions { text-align: left; margin: 30px auto; }
            #instructions textarea { width: 100%; height: 100px; }
            
            /* Show the controls (hidden at the start by default) */
            .video-js .vjs-control-bar { 
              display: -webkit-box;
              display: -webkit-flex;
              display: -ms-flexbox;
              display: flex;
            }
          
            /* Make the demo a little prettier */
            body {
              margin-top: 20px;
              background: #222;
              text-align: center; 
              color: #aaa;
              background: radial-gradient(#333, hsl(200,30%,6%) );
            }
          
            a, a:hover, a:visited { color: #76DAFF; }
          </style>

    <body>
  
    <div id="instructions">
            <video id="my_video_1" class="video-js vjs-default-skin" width="640px" height="267px"
                controls preload="none" poster=''
                data-setup='{ "aspectRatio":"640:267", "playbackRates": [1, 1.5, 2] }'>
            <source src='{{ url("$movie->url_video_serve") }}' type='video/mp4' />
            </video>             
    </div>

      <script src="{{ asset('js/player.js') }}"></script>

    </body>
</html>
