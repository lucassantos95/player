<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

     <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- Styles -->
    <link href="{{ asset('css/admin/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/material-dashboard.css') }}" rel="stylesheet">

</head>
<body class="">
  <div class="wrapper ">
  <div class="sidebar" data-color="purple" data-background-color="white" data-image="{{asset('imgs/sidebar-1.jpg') }}">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href=" {{ route('home') }} " class="simple-text logo-normal">
          Player Monstro
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item {{(Route::current()->getName() == 'home' ? 'active' : '') }}  ">
            <a class="nav-link" href="{{ route('home') }}">
              <i class="material-icons">movie</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item {{(Route::current()->getName() == 'video.create' ? 'active' : '') }} ">
            <a class="nav-link" href="{{ route('video.create') }} ">
              <i class="material-icons">cloud_upload</i>
              <p>Upload de Video</p>
            </a>
          </li>
        </ul>
      </div>
    </div>

     <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
         
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
           
            <ul class="navbar-nav">
             
              <li class="nav-item dropdown">
                <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                </div>
              </li>
 
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
        @yield('content')
      </div>
    </div>

  </div>

     <!--   Core JS Files   -->
    <script src="{{ asset('js/admin/main.js') }}"></script>
    <script src="{{ asset('js/admin/core/jquery.min.js') }}"></script>
    <script src="{{ asset('js/admin/core/popper.min.js') }}"></script>
    
    <!--  Notifications Plugin    -->
    <script src="{{ asset('js/admin/plugins/bootstrap-notify.js') }}"></script>
 
    <script src="{{ asset('js/admin/core/bootstrap-material-design.min.js') }}"></script>
    
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('js/admin/material-dashboard.js') }}"></script>

    <script src="{{ asset('js/admin/upload.js') }}"></script>
</body>
</html>
