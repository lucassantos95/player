<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\DownloadVideo;
use App\Model\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $localFile = storage_path() . 'movies';
        $image = file_get_contents('https://vjs.zencdn.net/v/oceans.mp4');
        file_put_contents(public_path('avatars') . 'video.mp4', $image);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Videos.upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function api_add(Request $request)
    {
        $dados = $request->all();
        $video = new Video;

        $video->titulo = $dados['titulo'];
        $video->url_to_download = $dados['video_url'];
        $video->status_id = 0;
        $videoSave = $video->save();
        DownloadVideo::dispatch($video);
        if ($videoSave) {

            $video->encryption_id = Crypt::encryptString($video->id);
            $video->save();
            $retorno['status'] = 201;
            $retorno['msg'] = 'criado com sucesso';
            echo json_encode($retorno);
        }
    }
    public function store(Request $request)
    {

        $dados = $request->all();

        $video = new Video;

        $video->titulo = $request->titulo;
        $video->url_to_download = $request->url_video;
        $video->status_id = 0;
        $videoSave = $video->save();
        DownloadVideo::dispatch($video);
        if ($videoSave) {
            return redirect('home/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function watch($id)
    {   

        $decrypt_id = Crypt::decryptString($id);
        $movie = Video::find($decrypt_id);
        return view('Site.watch', compact('movie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $id = 27;

        Video::downloadVideo($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
