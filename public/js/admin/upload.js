
var templete = '';

var urlBase = $('#url-base').val()+"/api";

function ajaxSendDados(titulo, video_url, index) {
    

    var urlRequest = urlBase + "/videos/add";
    console.log(urlRequest)
    $.ajax({
        type: "POST",
        url: urlRequest,
        data: { titulo: titulo, video_url: video_url },
        dataType: "json",
        success: function (response) {
            
            if (response.status == 201) {
                demo.showNotification('top','left');
            }
            $('#bloco-upload-' + index).html('');
           
        },
        error: function (xhr, ajaxOptions, thrownError) {
           
        }
    });
}

function getdados(index) {

    
    var titulo = $('#input-titulo-' + index).val();
    var video_url = $('#input-url-' + index).val();
    if (titulo != '') {
        if (video_url != '') {
            ajaxSendDados(titulo, video_url, index);
            
        } else {
            $('#input-url-' + index).focus();
            alert('Preencher campo URL');
        }
    } else {
        $('#input-titulo-' + index).focus();
        alert('Preencher campo titulo');
    }

}
function addNewUpload(index) {

    templete += '<div id="bloco-upload-' + index + '" >';
    templete += '<div class="row">';
    templete += '<div class="col-md-12">';
    templete += '<div class="form-group">';
    templete += '<label class="bmd-label-floating">Título</label>';
    templete += '<input id="input-titulo-' + index + '"  type="text" class="form-control">';
    templete += '</div>';
    templete += '</div>';
    templete += '<div class="col-md-12">';
    templete += '<div class="form-group">';
    templete += '<label class="bmd-label-floating">URL (.MP4)</label>';
    templete += '<input id="input-url-' + index + '"  type="text" class="form-control">';
    templete += '</div>';
    templete += '</div>';
    templete += '</div>';
    templete += '<button onclick="getdados(' + index + ')" class="btn btn-primary pull-right">Fazer Upload</button>';
    templete += '<div class="clearfix"></div>';
    templete += '<hr/>';
    templete += '</div>';

    $('#listagem-bloco-upload').append(templete);

    templete = '';
}


$('#new-video').on('click', function (envet) {
    var parameter = parseInt($(this).attr('contador'));
    console.log(parameter)
    addNewUpload(parameter);
    $(this).attr('contador', parameter + 1);
})