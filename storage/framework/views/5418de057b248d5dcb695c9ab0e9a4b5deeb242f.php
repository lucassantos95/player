<?php $__env->startSection('content'); ?>
        <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                  <h4>Videos</h4>
                </div>
                <div class="card-body">

                <div class="row thumb-row">

                  <?php $__currentLoopData = $videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <a href=" <?php echo e(URL::to('/videos/watch/'.$video->encryption_id)); ?> ">
                          <h5> <?php echo e($video->titulo); ?> </h5>
                      <div class="thumb-box">                       
                        <img style="width: 100%;height: 170px;" src="https://d2gg9evh47fn9z.cloudfront.net/800px_COLOURBOX14895857.jpg"/>
                   </div>
                   <div style="top: -10px;position: relative;text-align: right; padding-right: 7px;font-size: 15px;color: #da3155;font-weight: bold;">
                      <span id="status"> 
                          <?php if($video->status_id == 0): ?>
                            Processando
                         <?php endif; ?> 
                      </span>
                  </div>
                </a>
                </div>
              
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </div>
                  <?php echo e($videos->links()); ?>

              </div>
             
            </div>
          </div>
        </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>