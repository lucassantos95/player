<?php $__env->startComponent('mail::message'); ?>
# Boas Vindas, <?php echo e($user->name); ?>

 
Seja muito bem vindo a nossa plataforma 🙂
 
<?php $__env->startComponent('mail::button', ['url' => config('app.url')]); ?>
Acessar o Site
<?php echo $__env->renderComponent(); ?>
 
Obrigado,
<?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>