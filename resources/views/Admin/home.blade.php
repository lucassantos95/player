@extends('layouts.admin')

@section('content')
        <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                  <h4>Videos</h4>
                </div>
                <div class="card-body">

                <div class="row thumb-row">

                  @foreach ($videos as $video)
                
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <a href=" {{ URL::to('/videos/watch/'.$video->encryption_id) }} ">
                          <h5> {{ $video->titulo }} </h5>
                      <div class="thumb-box">                       
                        <img style="width: 80%;height: 170px;" src="https://d2gg9evh47fn9z.cloudfront.net/800px_COLOURBOX14895857.jpg"/>
                   </div>
                   <div style="top: -10px;position: relative;text-align: left; padding-right: 7px;font-size: 15px;color: #da3155;font-weight: bold;">
                      <span id="status"> 
                          @if($video->status_id == 0)
                            Processando
                         @endif 
                      </span>
                  </div>
                </a>
                </div>
              
                    @endforeach
                  </div>
                  {{ $videos->links() }}
              </div>
             
            </div>
          </div>
        </div>
@endsection
