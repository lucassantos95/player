<?php $__env->startSection('content'); ?>

          <div class="row">
            <div class="col-md-9">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Upload Remoto</h4>
                  <p class="card-category">Completo os Dados</p>
                </div>
                <div class="card-body">
                    <div id="listagem-bloco-upload" >
                        
                    </div>
                </div>
              </div>
            </div>
             <!--   fim col-md-9   -->
            <div class="col-md-3">
              <div class="card card-profile">
                <div class="card-body">
                  <a href="#" id="new-video" contador="0" class="btn btn-primary btn-round">Novo Video</a>
                </div>
              </div>
            </div>
            <!--   fim col-md-3   -->
          </div>
        </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>