<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::group(['middleware' =>['auth'], 'namespace' => 'Admin',], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('videos/create','VideosController@create')->name('video.create');
    Route::post('videos/store','VideosController@store')->name('video.store');
    Route::get('videos/edit','VideosController@edit')->name('video.edit');
    // Route::resource('videos', 'VideosController');
});

Route::get('videos/watch/{id}','Admin\VideosController@watch')->name('video.watch');

Route::group(['namespace' => 'Site',], function () {
    Route::get('/', 'SiteController@index')->name('welcome');
});

